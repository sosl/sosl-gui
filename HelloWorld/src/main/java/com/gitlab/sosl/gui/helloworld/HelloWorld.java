/* ****************************************************************************
 * Copyright (C) 2022 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * ***************************************************************************/

package com.gitlab.sosl.gui.helloworld;

import com.gitlab.mko575.jlogger.JLogger;
import com.gitlab.mko575.jlogger.subscribers.CLIErrorSubscriber;
import com.gitlab.mko575.jlogger.subscribers.CLIOutputSubscriber;
import com.gitlab.mko575.jlogger.tag.ELogCategory;
import com.gitlab.mko575.jlogger.tag.LogTag;
// import com.gitlab.sosl.gui.jfx.ForestryGUI;
import com.gitlab.sosl.interfaces.UserInterface;
import java.time.Duration;
import java.util.Properties;
import java.util.ServiceLoader;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;

public class HelloWorld {

    // private static void executeOneJFXUI() throws InterruptedException {
    //     final CountDownLatch closeLatch = new CountDownLatch(1);
    //     ForestryGUI gui = new ForestryGUI();
    //     gui.initUI(new Properties(), new DummyUIContext());
    //     gui.onExit(closeLatch::countDown);
    //     Thread.sleep(Duration.ofSeconds(2).toMillis());
    //     gui.addTestMenu();
    //     closeLatch.await();
    // }

    private static void executeAllUIs() throws InterruptedException {
        ServiceLoader<UserInterface> sl = ServiceLoader.load(UserInterface.class);
        int nbUIs = (int) sl.stream().count();
        System.out.printf("Found %d UIs\n", nbUIs);
        final CountDownLatch closeLatch = new CountDownLatch(nbUIs);
        sl.forEach(
            provider -> {
                UserInterface ui = provider;
                ui.initUI(new Properties(), new DummyUIContext());
                ui.onExit(closeLatch::countDown);
            }
        );
        closeLatch.await();
    }

    public static void main(String[] args) throws InterruptedException {
        JLogger.subscribe(
            new CLIOutputSubscriber("CLI output", null),
            // LogTag.get(ELogCategory.USAGE_LOG, Level.INFO)
            LogTag.get(ELogCategory.GENERIC_LOG, Level.ALL)
        );
        JLogger.subscribe(
            new CLIErrorSubscriber("CLI error", null),
            LogTag.get(ELogCategory.IMPLEMENTATION_LOG, Level.INFO)
        );
        JLogger.start();
        // executeOneJFXUI();
        executeAllUIs();
        JLogger.stop();
    }
}
