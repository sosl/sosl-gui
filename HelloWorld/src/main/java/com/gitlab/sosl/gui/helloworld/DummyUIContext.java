package com.gitlab.sosl.gui.helloworld;

import com.gitlab.sosl.interfaces.DataModel;
import com.gitlab.sosl.interfaces.UIContext;

public class DummyUIContext implements UIContext {

  @Override
  public DataModel getDataModel() {
    return null;
  }
}
