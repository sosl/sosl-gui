module sosl.gui.helloworld {
  // requires sosl.gui.jfx;
  requires sosl.interfaces;
  requires com.gitlab.mko.jlogger;
  requires java.logging; // Just for Level, it's a bit overkill ...

  uses com.gitlab.sosl.interfaces.UserInterface;
}
