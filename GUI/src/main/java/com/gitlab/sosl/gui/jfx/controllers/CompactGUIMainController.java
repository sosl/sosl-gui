package com.gitlab.sosl.gui.jfx.controllers;

import com.gitlab.mko575.jlogger.JLogger;
import com.gitlab.sosl.gui.jfx.Constants;
import com.gitlab.sosl.interfaces.UIContext;
import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class CompactGUIMainController implements MainUIController {

  @FXML
  private MenuBar mainMenuBar;

  @FXML
  private SplitPane explorersPane;

  private final FXMLLoader rootLoader;

  private final Set<UIController> subControllers = new HashSet<>();

  private final UIContext uiContext;

  public CompactGUIMainController(FXMLLoader loader, UIContext uic) {
    this.rootLoader = loader;
    this.uiContext = uic;
  }

  protected UIContext getUIContext() {
    return uiContext;
  }

  protected void addSubController(UIController subc) {
    Objects.requireNonNull(subc);
    subControllers.add(subc);
  }

  private FXMLLoader getLoaderFor(URL fxmlURL) {
    FXMLLoader loader = new FXMLLoader();
    loader.setLocation(fxmlURL);
    loader.setResources(rootLoader.getResources());
    loader.setBuilderFactory(rootLoader.getBuilderFactory());
    loader.setControllerFactory(rootLoader.getControllerFactory());
    loader.setCharset(rootLoader.getCharset());
    return loader;
  }

  @Override
  public CompactGUIMainController addTestMenu() {
    FXMLLoader loader = getLoaderFor(Constants.FXML_TEST_MENU);
    try {
      Menu testMenu = loader.load();
      Platform.runLater(() -> mainMenuBar.getMenus().add(testMenu));
    } catch (IOException e) {
      JLogger.implementationError("Failed to add Explorer: %s", e.getMessage());
    }
    return this;
  }

  /**
   * Opens the provided content in a new window with the provided title.
   * @param title the title of the new window.
   * @param content the content of the new window.
   */
  private void openInNewWindow(String title, Node content) {
    final StackPane layout = new StackPane(content);
    final Scene scene = new Scene(layout);
    final Stage window = new Stage();
    window.setTitle(title);
    window.setScene(scene);
    window.show();
  }

  @Override
  public CompactGUIMainController addExplorer(boolean inNewWindow) {
    FXMLLoader loader = getLoaderFor(Constants.FXML_EXPLORER);
    Node explorer = null;
    UIController controller = null;
    try {
      explorer = loader.load();
      controller = loader.getController();
      if (controller instanceof ExplorerController ec)
        ec.addDummyData();
    } catch (IOException e) {
      JLogger.implementationError("Failed to load Explorer: %s", e.getMessage());
    }
    if ( explorer != null ) {
      if (inNewWindow) {
        openInNewWindow("Explorer", explorer);
        JLogger.implementationInfo(Level.FINE, "Created new explorer window.");
      } else {
        if (explorersPane != null) {
          if (controller instanceof ExplorerController ec) {
            ec.addCloseButton(explorer, explorersPane);
          }
          explorersPane.getItems().add(explorer);
          JLogger.implementationInfo(Level.FINE, "Adding explorer tree view to GUI.");
        } else {
          JLogger.debug(Level.SEVERE, "ExplorersPane is NULL?");
        }
      }
    }
    return this;
  }

}
