package com.gitlab.sosl.gui.jfx.controllers;

import com.gitlab.mko575.jlogger.JLogger;
import com.gitlab.sosl.interfaces.UIContext;
import java.util.logging.Level;
import javafx.fxml.FXMLLoader;
import javafx.util.Callback;

public class ForestryControllersFactory implements
    Callback<Class<?>, Object> {

  private final FXMLLoader loader;

  private final UIContext uiContext;

  private CompactGUIMainController mainController;

  public ForestryControllersFactory(FXMLLoader loader, UIContext uic) {
    this.loader = loader;
    this.uiContext = uic;
  }

  @Override
  public UIController call(Class<?> controllerClass) {
    JLogger.implementationInfo(Level.FINE,
        String.format("Creating controller for %s.", controllerClass)
    );
    UIController controller = null;
    if ( controllerClass == CompactGUIMainController.class ) {
      mainController = new CompactGUIMainController(loader, uiContext);
      controller = mainController;
    } else if ( controllerClass == MainMenuController.class ) {
      controller = new MainMenuController(mainController);
      mainController.addSubController(controller);
    } else if ( controllerClass == TestMenuController.class ) {
      controller = new TestMenuController(mainController);
      mainController.addSubController(controller);
    } else if ( controllerClass == ExplorerController.class ) {
      controller = new ExplorerController(mainController);
      mainController.addSubController(controller);
    } else {
      JLogger.implementationError(
          "Error in %s, missing implementation in 'call' for controller class %s",
          this.getClass(), controllerClass
      );
    }
    return controller;
  }
}
