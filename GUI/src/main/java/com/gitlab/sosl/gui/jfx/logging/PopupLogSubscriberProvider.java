package com.gitlab.sosl.gui.jfx.logging;

import com.gitlab.mko575.jlogger.subscribers.ISubscriberProvider;
import java.util.Optional;

public class PopupLogSubscriberProvider
    implements ISubscriberProvider<PopupLogSubscriber> {

  private static final Optional<PopupLogSubscriber> subscriber;

  static {
      subscriber = Optional.of(new PopupLogSubscriber());
  }

  @Override
  public Optional<PopupLogSubscriber> getSubscriber() {
    return subscriber;
  }
}
