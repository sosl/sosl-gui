/* ****************************************************************************
 * Copyright (C) 2022 MKO575
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * ***************************************************************************/

package com.gitlab.sosl.gui.jfx;

import com.gitlab.mko575.jlogger.JLogger;
import com.gitlab.sosl.gui.jfx.controllers.ForestryControllersFactory;
import com.gitlab.sosl.gui.jfx.controllers.MainUIController;
import com.gitlab.sosl.interfaces.UIContext;
import com.gitlab.sosl.interfaces.UserInterface;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Main class handling Forestry's GUI.
 *
 * @see Application
 * @see Platform
 * @author M.K.O. 575 @ <a href="https://gitlab.com/mko575">https://gitlab.com/mko575</a>}
 */
public class ForestryGUI implements UserInterface {

  /**
   * The {@link UIContext} this GUI interacts with.
   */
  private UIContext uiContext;

  /**
   * The main stage/window of this Forestry's GUI.
   */
  private Stage mainStage; // NOPMD : SonarLint is smarter

  /**
   * The main controller for Forestry's GUI.
   */
  private MainUIController mainController;

  @Override
  public void initUI(Properties prop, UIContext uic) {
    this.uiContext = uic;
    final CountDownLatch startupLatch = new CountDownLatch(1);
    try {
      Platform.startup(startupLatch::countDown);
      // Wait for the JavaFX Application Thread to be started
      try { startupLatch.await(); }
      catch (InterruptedException e) {
        JLogger.debug(
            Level.WARNING,
            "Interrupted while waiting for the JavaFX Application Thread to be started."
                + " I will try to continue anyway, but further exceptions may be triggered."
        );
      }
    } catch (IllegalStateException e) {
      JLogger.debug(Level.FINE, "JavaFX platform already started.");
    }
    Platform.runLater(() -> this.createMainStage().show());
  }

  /**
   * Create and returns the main {@link Stage} for the GUI.
   * This method has to be called inside {@link Platform#runLater(Runnable)}.
   *
   * @return the main GUI stage/window
   */
  private Stage createMainStage() {
    FXMLLoader loader = new FXMLLoader();
    loader.setLocation(Constants.FXML_MAIN_STAGE);
    // loader.setResources(null);
    // loader.setBuilderFactory(new JavaFXBuilderFactory());
    loader.setControllerFactory(new ForestryControllersFactory(loader, uiContext));
    loader.setCharset(StandardCharsets.UTF_8);

    mainStage = new Stage();
    try {
      mainStage.setScene(new Scene(loader.load()));
      mainController = loader.getController();
      mainController.addExplorer(false);
    } catch (Throwable e) {
      JLogger.debug(
          Level.SEVERE,
          "Error while populating the main stage from file %s: %s",
          Constants.FXML_MAIN_STAGE.toString(), e.getMessage()
      );
      mainStage = createMainStageBackup(Optional.of(e));
    }

    mainStage.getScene().getStylesheets().add(Constants.STYLEHEET_ICONS_MATERIAL);
    mainStage.sizeToScene();
    mainStage.setTitle("Forestry's Main Stage");
    mainStage.setOnCloseRequest(e -> Platform.runLater(Platform::exit));
    
    return mainStage;
  }

  /**
   * Create and returns the backup {@link Stage} for the GUI.
   * This method has to be called inside {@link Platform#runLater(Runnable)}.
   *
   * @return the main GUI stage/window
   */
  private Stage createMainStageBackup(Optional<Throwable> eOpt) {
    final VBox mainLayout = new VBox();
    final String javaVersion = System.getProperty("java.version");
    final String javafxVersion = System.getProperty("javafx.version");
    final Label label = new Label(
        "Running JavaFX " + javafxVersion
        + " on Java " + javaVersion + ".");
    label.setPadding(new Insets(5));
    mainLayout.getChildren().add(label);
    if (eOpt.isPresent()) {
      final Throwable e = eOpt.get();
      final ByteArrayOutputStream baos = new ByteArrayOutputStream();
      final String utf8 = StandardCharsets.UTF_8.name();
      String stackTraceStr = "There should be a stack trace here!";
      try (PrintStream ps = new PrintStream(baos, true, utf8)) {
        e.printStackTrace(ps);
        stackTraceStr = baos.toString(utf8);
      } catch (Exception drop) {}
      final Label exceptionLabel = new Label(stackTraceStr);
      exceptionLabel.setPadding(new Insets(5));
      mainLayout.getChildren().add(exceptionLabel);
    }
    mainLayout.autosize();
    final Scene scene = new Scene(mainLayout);

    mainStage = new Stage();
    mainStage.setScene(scene);
    mainStage.sizeToScene();
    mainStage.setTitle("Forestry's Main Stage");
    mainStage.setOnCloseRequest(e -> Platform.runLater(Platform::exit));
    return mainStage;
  }

  @Override
  public void onExit(Runnable r) {
    Platform.runLater(() -> {
      final EventHandler<WindowEvent> onCloseEvent = mainStage.getOnCloseRequest();
      mainStage.setOnCloseRequest(e -> {
        r.run();
        onCloseEvent.handle(e);
      });
    });
  }

  /*
  public static ForestryGUI get() {
    ForestryGUI gui = initGUI();
    try {
      Thread.sleep(Duration.ofSeconds(5).toMillis());
      Platform.runLater(() -> gui.mainController.addExplorer());
      Thread.sleep(Duration.ofSeconds(2).toMillis());
      Platform.runLater(() -> gui.mainController.addExplorer());
    } catch (InterruptedException e) {
      JLogger.implementationError(Level.FINEST, "Stopped while sleeping!");
    }
    return gui;
  }
   */

  /**
   * Adds a test menu to the GUI. This menu has no utility apart from
   * development and debugging.
   * @return this GUI to chain calls.
   */
  public ForestryGUI addTestMenu() {
    mainController.addTestMenu();
    return this;
  }
}
