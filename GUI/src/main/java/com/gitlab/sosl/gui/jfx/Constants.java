package com.gitlab.sosl.gui.jfx;

import java.net.URL;
import java.util.Objects;

public class Constants {

  /**
   * Retrieves the URL of the resource file whose resource path is provided.
   * @param resourcePath the resource path to convert.
   * @return a URL corresponding to the provided resource path.
   */
  private static URL getFXML(String resourcePath) {
    return Constants.class.getResource(resourcePath);
  }

  /**
   * Resource path of the FXML file describing the main GUI stage/window.
   */
  public static final URL FXML_MAIN_STAGE =
      getFXML("/JavaFX/ForestryGUI.fxml");

  public static final URL FXML_EXPLORER =
      getFXML("/JavaFX/Explorer.fxml");

  public static final URL FXML_TEST_MENU =
      getFXML("/JavaFX/TestMenu.fxml");

  /**
   * Retrieves a string representation of the external URL for the resource file
   * whose path is provided.
   * @param resourcePath the resource path to convert.
   * @return a string representation of the external URL corresponding to the provided
   * resource path.
   */
  private static String getStylesheet(String resourcePath) {
    final String res =
        Objects.requireNonNull(
            Constants.class.getResource(resourcePath)
        ).toExternalForm();
    return res;
  }

  /**
   * External path of the CSS file setting usage of Material icons.
   */
  public static final String STYLEHEET_ICONS_MATERIAL =
      getStylesheet("/JavaFX/stylesheets/ForestryGUI-Icons-Material.css");

}
