package com.gitlab.sosl.gui.jfx.controllers;

import com.gitlab.sosl.gui.jfx.viewModels.ExplorerViewModel;
import com.gitlab.sosl.interfaces.UIContext;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.ToolBar;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import org.kordamp.ikonli.javafx.FontIcon;

public class ExplorerController implements UIController {

  @FXML
  private ToolBar explorerToolBar;

  @FXML
  private TreeView explorerTreeView;

  private final CompactGUIMainController mainController;

  private final UIContext uiContext;

  private final ExplorerViewModel viewModel;

  public ExplorerController (
      CompactGUIMainController mainController
  ) {
    this.mainController = mainController;
    this.uiContext = mainController.getUIContext();
    this.viewModel = new ExplorerViewModel(uiContext.getDataModel());
  }

  /**
   * At the end of the explorer toolbar, adds a button to remove this explorer from its parent.
   * @param explorer the explorer controlled by this controller.
   * @param parent the explorer's container.
   */
  public void addCloseButton(Node explorer, SplitPane parent) {
    Label closeButton = new Label();
    closeButton.setGraphic(new FontIcon());
    closeButton.getStyleClass().add("close-button");
    closeButton.setOnMouseClicked( e ->
        parent.getItems().remove(explorer)
    );
    explorerToolBar.getItems().add(closeButton);
  }

  public void addDummyData() {
    TreeItem<String> rootTI = new TreeItem<>("/");
    explorerTreeView.setRoot(rootTI);
    rootTI.getChildren().add(new TreeItem<>("TreeItem"));
  }

}
