package com.gitlab.sosl.gui.jfx.controllers;

public interface MainUIController extends UIController {

  /**
   * Adds a test menu to the GUI. This menu has no utility apart from
   * development and debugging.
   * @return this UI controller to chain calls.
   */
  MainUIController addTestMenu();

  /**
   * Adds an explorer component to this GUI.
   * @param inNewWindow if {@code true}, opens the explorer in a new window;
   *                    otherwise, opens it in the main window.
   * @return this UI controller to chain calls.
   */
  MainUIController addExplorer(boolean inNewWindow);
}
