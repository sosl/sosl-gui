package com.gitlab.sosl.gui.jfx.controllers;

import com.gitlab.sosl.interfaces.UIContext;
import javafx.fxml.FXML;

public class TestMenuController implements UIController {

  private final CompactGUIMainController mainController;

  private final UIContext uiContext;

  public TestMenuController(
      CompactGUIMainController mainController
  ) {
    this.mainController = mainController;
    this.uiContext = mainController.getUIContext();
  }

  public void callAddExplorer(boolean inNewWindow) {
    mainController.addExplorer(inNewWindow);
  }

  @FXML
  private void addExplorerInMainWindow() {
    callAddExplorer(false);
  }

  @FXML
  private void addExplorerInNewWindow() {
    callAddExplorer(true);
  }

}
