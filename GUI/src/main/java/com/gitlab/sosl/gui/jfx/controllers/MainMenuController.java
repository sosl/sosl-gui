package com.gitlab.sosl.gui.jfx.controllers;

import com.gitlab.sosl.gui.jfx.viewModels.MainMenuViewModel;
import com.gitlab.sosl.interfaces.UIContext;

public class MainMenuController implements UIController {

  private final CompactGUIMainController mainController;

  private final UIContext uiContext;

  private final MainMenuViewModel viewModel;

  public MainMenuController(
      CompactGUIMainController mainController
  ) {
    this.mainController = mainController;
    this.uiContext = mainController.getUIContext();
    this.viewModel = new MainMenuViewModel(uiContext.getDataModel());
  }

}
