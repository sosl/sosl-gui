package com.gitlab.sosl.gui.jfx.logging;

import com.gitlab.mko575.jlogger.JLogger;
import com.gitlab.mko575.jlogger.LogEntry;
import com.gitlab.mko575.jlogger.subscribers.ILogSubscriber;
import com.gitlab.mko575.jlogger.tag.ELogCategory;
import com.gitlab.mko575.jlogger.tag.LogTag;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class PopupLogSubscriber implements ILogSubscriber {

  public static final boolean oneAtATime = true;

  private static final Semaphore semaphore = new Semaphore(1);

  private static final Map<Level, AlertType> logLvl2alertType = Map.of(
      Level.SEVERE, AlertType.ERROR,
      Level.WARNING, AlertType.WARNING,
      Level.INFO, AlertType.INFORMATION
  );

  @Override
  public LogTag getDefaultSubscriptionTopic() {
    return LogTag.get(ELogCategory.USAGE_LOG, Level.SEVERE);
  }

  @Override
  public void onNext(final LogEntry logEntry) {
    try {
      if ( oneAtATime ) semaphore.acquire();
      Platform.runLater(() -> {
        final Level logLvl = logEntry.getTag().getLevel();
        final Alert alert = new Alert(logLvl2alertType.getOrDefault(logLvl, AlertType.INFORMATION));
        alert.setTitle("Log Message");
        alert.setHeaderText(String.format(
            "%s  message", logLvl.getLocalizedName()
        ));
        alert.setContentText(logEntry.getMessage());
        if ( oneAtATime ) {
          alert.showAndWait();
          semaphore.release();
        } else {
          alert.show();
        }
      });
    } catch (InterruptedException e) {
      JLogger.debug(
          Level.FINEST,
          "PopupLogSubscriber is giving up on a log message due to interruption."
      );
    }
  }
}
