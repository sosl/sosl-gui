import com.gitlab.sosl.gui.jfx.ForestryGUI;

module sosl.gui.jfx {
  requires java.desktop;
  requires javafx.controls;
  requires javafx.swing;
  requires javafx.fxml;

  requires org.kordamp.ikonli.core;
  requires org.kordamp.ikonli.javafx;
  requires org.kordamp.ikonli.fontawesome5;
  requires org.kordamp.ikonli.materialdesign2;

  // requires com.github.vlsi.mxgraph.jgraphx;

  requires com.gitlab.mko.jlogger;
  requires java.logging; // Just for Level, it's a bit overkill ...

  requires sosl.interfaces;

  opens com.gitlab.sosl.gui.jfx to javafx.fxml;
  opens com.gitlab.sosl.gui.jfx.controllers to javafx.fxml;

  provides com.gitlab.sosl.interfaces.UserInterface
      with ForestryGUI;
  provides com.gitlab.mko575.jlogger.subscribers.ISubscriberProvider
      with com.gitlab.sosl.gui.jfx.logging.PopupLogSubscriberProvider;

  exports com.gitlab.sosl.gui.jfx;
  exports com.gitlab.sosl.gui.jfx.logging;
}
