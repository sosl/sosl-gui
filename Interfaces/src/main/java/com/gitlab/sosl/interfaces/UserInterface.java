package com.gitlab.sosl.interfaces;

import java.util.Properties;

public interface UserInterface {

  /**
   * Initializes the interface with the provided properties and UI context and provides
   * (displays) it to the user.
   * @param prop properties containing the GUI configuration.
   * @param uic the UI context this GUI interacts with (containing the data model and
   *            synchronizing UIs).
   */
  void initUI(Properties prop, UIContext uic);

  /**
   * Registers an action to be performed on exiting the UI. The action is executed before
   * every other already registered exit actions.
   * @param r the action to be performed.
   */
  void onExit(Runnable r);

}
