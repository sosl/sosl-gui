package com.gitlab.sosl.interfaces;

public interface UIContext {

  /**
   * Returns the data model the user is interacting with when using this UI context.
   * @return the data model associated to this UI context.
   */
  DataModel getDataModel();

}
